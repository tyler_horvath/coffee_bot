import os, time
import hipchat
import usb.core
import usb.util
import pygtk
pygtk.require('2.0')
import gtk
from sys import exit
import math
from datetime import datetime,timedelta

#devops,stella,tech
rooms = ['367393','552001','250098']

from pymongo import MongoClient
import usb.core

username = "rpi"
password = "passwordhere"

connection = MongoClient("mongodb://{}:{}@candidate.19.mongolayer.com:10810/coffee".format(username,password))

# connect to the students database and the ctec121 collection
db = connection.coffee.coffee_data


chat = hipchat.HipChat(token="1d1526b200fa796d0003988c76c8d1")
hipchat_message = chat.message_room
# DYMO M25
VENDOR_ID = 0x0922
PRODUCT_ID = 0x8004


#find USB
dev = usb.core.find(idVendor=VENDOR_ID,
                       idProduct=PRODUCT_ID)
def main():
   try:
        # was it found?
      if dev is None:
            print "device not found"
            exit()
      else:
            devmanufacturer = usb.util.get_string(dev, 1)
            devname = usb.util.get_string(dev, 2)
            print "device found: " + devmanufacturer + " " + devname
 
            interface = 0
            if dev.is_kernel_driver_active(interface) is True:
                print "but we need to detach kernel driver"
                dev.detach_kernel_driver(interface)
 
                # use the first/default configuration
                dev.set_configuration()
                print "claiming device"
                usb.util.claim_interface(dev, interface)

      calibration = calibrate()
      empty_weight = calibration[0]
      full_weight = calibration[1]
      while True:
            stable_weight = listen()
            timestamp = datetime.now()
            db.insert({"time":timestamp,"mass":stable_weight})
            #print stable_weight
            print empty_weight,stable_weight,full_weight
            #if stable_weight < empty_weight + 20:
            #   print "I see empty"
            #   send_email("empty")
            #if stable_weight > full_weight:
            #   print "I see full"
            #   send_email("fresh")
 
   except KeyboardInterrupt as e: 
      print "\nquitting"
      exit();

def calibrate():
   full_weight = 100
   print "Full Weight set to {} grams".format(full_weight)
   time.sleep(10)
   empty_weight = 0
   print "Empty Weight set to {} grams".format(empty_weight)
   #full_weight = listen()
   #time.sleep(10)
   #empty_weight = listen()
   return [empty_weight,full_weight]
 
def random_phrase(status):
   return status

def send_email(payload, send_bool):
   message = payload
   #message = random_phrase(payload)
   current_time = datetime.now()
   time_buffer = timedelta(minutes=1)
   if send_bool == True:
      if payload == "empty":
         hipchat_message(rooms[0], "Coffee Bot", message, color="red")#, notify=True)
      if payload == "fresh":
         hipchat_message(rooms[0], "Coffee Bot", message, color="purple")#, notify=True) 
      return False
   return True



def grab():
        # first endpoint
        endpoint = dev[0][(0,0)][0]
        # read a data packet
        attempts = 10
        data = None
        while data is None and attempts > 0:
            try:
                data = dev.read(endpoint.bEndpointAddress,
                                   endpoint.wMaxPacketSize)
            except usb.core.USBError as e:
                data = None
                if e.args == ('Operation timed out',):
                    attempts -= 1
                    print "timed out... trying again"
                    continue
 
        return data
 
def listen():
    DATA_MODE_GRAMS = 2
    DATA_MODE_OUNCES = 11
 
    last_raw_weight = 0
    last_raw_weight_stable = 4
 
    print "listening for weight..."
 
    while True:
        time.sleep(.5)
 
        weight = 0
        print_weight = ""
 
        data = grab()
        if data != None:
            raw_weight = data[4] + data[5] * 256
 
        # +/- 2g
        if raw_weight > 0 and abs(raw_weight-last_raw_weight) > 0 and raw_weight != last_raw_weight:
            last_raw_weight_stable = 4
            last_raw_weight = raw_weight
 
        if raw_weight > 0 and last_raw_weight_stable >= 0:
            last_raw_weight_stable -= 1
 
        if raw_weight > 0 and last_raw_weight_stable == 0:
            if data[2] == DATA_MODE_OUNCES:
                ounces = raw_weight * 0.1
                weight = math.ceil(ounces)
                print_weight = "%s oz" % ounces
            elif data[2] == DATA_MODE_GRAMS:
                grams = raw_weight
                weight = math.ceil(grams)
                print_weight = "%s g" % grams
 
            print "stable weight: " + print_weight
 
            return raw_weight
 
 
def probe():
    for cfg in dev:
        print "cfg: " + str(cfg.bConfigurationValue)
        print "descriptor: " + str(usb.util.find_descriptor(cfg, find_all=True, bInterfaceNumber=1))
        for intf in cfg:
            print "interfacenumber, alternatesetting: " + str(intf.bInterfaceNumber) + ',' + str(intf.bAlternateSetting)
            for ep in intf:
                print "endpointaddress: " + str(ep.bEndpointAddress)
 
 
#probe()
main()
