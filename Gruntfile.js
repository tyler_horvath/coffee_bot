module.exports = function(grunt) {
    grunt.initConfig({
       concat: {
           options: {
               separator: '\n'
           },
           distjs: {
               src: [
                   'bower_components/jquery/dist/jquery.min.js',
                   'bower_components/angular/angular.min.js',
                   'bower_components/angular-bootstrap/ui-bootstrap.min.js',
                   'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                   'bower_components/angular-ui-grid/ui-grid.min.js',
                   'bower_components/angular-ui-router/release/angular-ui-router.min.js',
                   'bower_components/angular-google-chart/ng-google-chart.js',
                   'bower_components/datatables/media/js/jquery.dataTables.min.js',
                   'bower_components/datatables/media/js/jquery.dataTables.min.js',
                   'bower_components/flot/excanvas.min.js',
                   'bower_components/flot/jquery.flot*',
                   'bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js',
                   'bower_components/holderjs/holder.js',
                   'bower_components/metisMenu/dist/metisMenu.min.js',
                   'bower_components/mocha/mocha.js',
                   'bower_components/morrisjs/morris.min.js',
                   'bower_components/raphael/raphael-min.js',
                   'bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js',
                   'static/app/app.min.js'

               ],
               dest: 'static/assets/js/built.min.js'
           },
           distcss: {
               src: [
                   'static/assets/css/base.min.css',
                   'bower_components/bootstrap/dist/css/bootstrap.min.css',
                   'bower_components/bootstrap-social/bootstrap-social.css',
                   'bower_components/datatables/media/css/jquery.dataTables.min.css',
                   'bower_components/datatables-responsive/css/dataTables.responsive.css',
                   'bower_components/metisMenu/dist/metisMenu.min.css',
                   'bower_components/mocha/mocha.css',
                   'bower_components/morrisjs/morris.css',
                   'bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css',
                   'bower_components/startbootstrap-sb-admin-2/dist/css/timeline.css',
                   'bower_components/components-font-awesome/css/font-awesome.min.css',
                   'bower_components/angular-ui-grid/ui-grid.min.css',
                   'bower_components/components-font-awesome/fonts/*.*'
               ],
               dest: 'static/assets/css/built.min.css'
           }
       },
       uglify: {
           my_target: {
               files: {
                   'static/app/app.min.js': ['static/app/app.js']
               }
           }
       },
       cssmin: {
           target: {
               files: {
                    'static/assets/css/base.min.css': ['static/assets/css/base.css']
               }
           }
       },
       watch: {
           files: ['static/app/app.js'],
           tasks: ['uglify', 'cssmin', 'concat']
       },
        // This will copy the fonts for font awesome out to the root static directory, which is needed when the
        // the css is minified
       copy: {
           main: {
               files: [
                   {expand: true, flatten: true, cwd:'static/bower_components/components-font-awesome/fonts', src: '*', dest: 'static/fonts'}
               ]
           }
       }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['uglify', 'cssmin', 'copy', 'concat'])

};