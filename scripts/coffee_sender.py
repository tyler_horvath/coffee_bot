from datetime import datetime, timedelta
import os, time, multiprocessing
from pymongo import MongoClient
import usb.core
import usb.util
import gtk
from sys import exit
import math
import hipchat
hipster = hipchat.HipChat(token="")


STELLA = 552001
DEVOPS = 367393
from_name = 'CoffeeBot'
message = "Testing"
message_color = 'purple'

name_to_serial = {"0081446057541":"Ren",
                  "0081446059603":"Stimpy"
                  }

username = "rpi"
password = "lols"

connection = MongoClient("mongodb://{}:{}@candidate.19.mongolayer.com:10810/coffee".format(username,password))

# connect to the coffee database and the coffee_data collection
db = connection.coffee.coffee_data

VENDOR_ID = 0x0922
PRODUCT_ID = 0x8004

#find USB
devices = list(usb.core.find(find_all=True,idVendor=VENDOR_ID,
                    idProduct=PRODUCT_ID))


class Scale(object):
    def __init__(self):
        self.current_status = -1
        self.previous_status = -1
        self.current_weight = -1
        self.last_weight = -2
        self.stable_weight = -3

    def insert(self, weight):
        timestamp = datetime.now()
        db.insert({"time": timestamp, "mass": weight})

    def main(self,dev):
        
        try:
        # was it found?
            if dev is None:
                print "device not found"
                exit()
            else:
                devmanufacturer = usb.util.get_string(dev, 1)
                devname = usb.util.get_string(dev, 2)
                serial = usb.util.get_string(dev,3)
                print "device found: " + devmanufacturer + " " + devname
                interface = 0
                if dev.is_kernel_driver_active(interface) is True:
                    print "but we need to detach kernel driver"
                    dev.detach_kernel_driver(interface)
                    # use the first/default configuration
                    dev.set_configuration()
                    print "claiming device"
                    usb.util.claim_interface(dev, interface)
            while True:
                    serial = usb.util.get_string(dev,3)
                    self.stable_weight = self.listen(dev)
                    #if self.stable_weight != self.last_weight:
                    timestamp = datetime.now()
                    fresh = self.check_status(serial)
                    #if not self.stable_weight == -3:
                    try:
                        db.insert({"time": timestamp,"mass": self.stable_weight,"serial":serial,"fresh":fresh})
                    except:
                        print "Couldn't write to DB"
                    print "Weight from {} written".format(serial)
                    time.sleep(.5)
                    print "Checking Status Change"
                    
                    #self.last_weight = self.stable_weight


        except KeyboardInterrupt as e:
            print "\nquitting"
            exit();

    def grab(self,dev):
        # first endpoint
        endpoint = dev[0][(0,0)][0]
        # read a data packet
        attempts = 10
        data = None
        while data is None and attempts > 0:
            try:
                data = dev.read(endpoint.bEndpointAddress,
                                endpoint.wMaxPacketSize)
            except usb.core.USBError as e:
                data = None
                if e.args == ('Operation timed out',):
                    attempts -= 1
                    print "timed out... trying again"
                    continue

        return data

    def listen(self,dev):
        DATA_MODE_GRAMS = 2
        DATA_MODE_OUNCES = 11

        last_raw_weight = 0
        last_raw_weight_stable = 4

        print "listening for weight..."
        while True:
                time.sleep(.5)
                data = self.grab(dev)
                if data != None:
                    raw_weight = data[4] + data[5] * 256

                # +/- 2g
                if raw_weight > 0 and abs(raw_weight-last_raw_weight) > 0 and raw_weight != last_raw_weight:
                    last_raw_weight_stable = 4
                    last_raw_weight = raw_weight

                if raw_weight > 0 and last_raw_weight_stable >= 0:
                    last_raw_weight_stable -= 1

                if raw_weight > 0 and last_raw_weight_stable == 0:
                    if data[2] == DATA_MODE_OUNCES:
                        raw_weight = raw_weight * 2.83495
                    elif data[2] == DATA_MODE_GRAMS:
                        raw_weight = raw_weight

                    return raw_weight


    def check_status(self,serial):
        #day_history = history("day")
        print "checking ",name_to_serial[serial]
        fivemin_history = self.history("local",serial)
        self.current_status = self.analyze(fivemin_history)
        #print self.current_status
        if self.current_status == -1:
            print "need more data, just booted"
            pass
        if self.current_status == 0:
            if self.previous_status == -1:
                ## Just booted and have empty container"
                message = "I'm alive! Lets make coffee!"
                hipster.message_room(DEVOPS, name_to_serial[serial], message, color=message_color, notify=False)
            if self.previous_status == 0:
                ## Still Empty
                print "Still empty..."  
            if self.previous_status == 1:
                ##Legitimate Empty - Notify
                message =  "Yeah we're dry over here - {}".format(name_to_serial[serial]) 
                hipster.message_room(STELLA, name_to_serial[serial], message, color=message_color, notify=True)

            if self.previous_status == 2:
                ## From full to empty means something funky happened, probably
                print "We were just full - something might not be right"

        if self.current_status == 1:
            if self.previous_status == -1:
                ## Just booted and have partially empty container"
                message = "I'm alive! Lets make coffee! But rinse me out first"
                hipster.message_room(DEVOPS, name_to_serial[serial], message, color=message_color, notify=False)
            if self.previous_status == 0:
                ## Partially filled - shouldn't be a real state...
                print "Somethings not right - I'm only half full!"
            if self.previous_status == 1:
                ## Still just sitting here with coffee
                print "No one is drinking me..."
            if self.previous_status == 2:
                ## From full to low is the expected transistion 
                message = "We're getting low - keep an eye on me! - {}".format(name_to_serial[serial])
                hipster.message_room(STELLA, name_to_serial[serial], message, color=message_color, notify=False)

        if self.current_status == 2:
            if self.previous_status == -1:
                ## Just booted and have full container"
                message = "I'm alive! I've got coffee! But it might be old..."
                hipster.message_room(DEVOPS, name_to_serial[serial], message, color=message_color, notify=False)
            if self.previous_status == 0:
                ## refilled - this is correct state transistion
                message = "What once was empty now is full - all is right in this world. - {}".format(name_to_serial[serial])
                hipster.message_room(STELLA, name_to_serial[serial], message, color=message_color, notify=True)
                self.previous_status = self.current_status
                return True  
            if self.previous_status == 1:
                ## Could have been old/stale - this is a potentially legit transistion
                print "I've got fresh coffee!"
            if self.previous_status == 2:
                ## From full to full 
                print "All this fresh coffee and no one is drinking me!"
        self.previous_status = self.current_status
        return False

        
    def history(self,timeframe,serial):

        eod = datetime.now()
        eod = eod.replace(hour=23,minute=0,second=0,microsecond=0)
        sod = eod
        sod = sod.replace(hour=5)

        if timeframe == "local":
            day_history = []
            now = datetime.now()
            then = now - timedelta(minutes=5)
            for item in db.find({"serial":serial,"time": {"$lt": now, "$gt":then}}).sort("time"):
                day_history.append([item["time"],item["mass"]])
            while len(day_history) == 0:
                #keep going back 5 minutes at a time till you find a value
                then = then - timedelta(minutes=5)
                for item in db.find({"time": {"$lt": now, "$gt":then}}).sort("time"):
                    day_history.append([item["time"],item["mass"]])
            #print day_history
            return day_history



    def analyze(self,hist_list):
        hist_list_rev = hist_list 
        hist_list_rev.reverse()
        if hist_list_rev[0][1] <= 2200:
            print "Probably Empty"
            return 0
        if hist_list_rev[0][1] <= 2600 and hist_list_rev[0][1] > 2200:
            print "We're Getting Low"
            return 1
        if hist_list_rev[0][1] >= 2600:
            print "We're definiely full"
            return 2


    #probe()
if __name__ == '__main__':
    scale = Scale()
    process1 = multiprocessing.Process(target=scale.main, args=(devices[0],))
    process2 = multiprocessing.Process(target=scale.main, args=(devices[1],))
    process1.start()
    process2.start()
