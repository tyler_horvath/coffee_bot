from datetime import datetime, timedelta
import os, time
from pymongo import MongoClient
import usb.core
import usb.util
import gtk
from sys import exit
import math

username = "rpi"
password = "passwordhere"

connection = MongoClient("mongodb://{}:{}@candidate.19.mongolayer.com:10810/coffee".format(username,password))

# connect to the students database and the ctec121 collection
db = connection.coffee.coffee_data

VENDOR_ID = 0x0922
PRODUCT_ID = 0x8004

#find USB
dev = usb.core.find(idVendor=VENDOR_ID,
                    idProduct=PRODUCT_ID)


class Scale(object):
    def __init__(self):
        self.current_weight = -1
        self.last_weight = -2
        self.stable_weight = -3

    def insert(self, weight):
        timestamp = datetime.now()
        db.insert({"time": timestamp, "mass": weight})

    def main(self):

        try:
        # was it found?
            if dev is None:
                print "device not found"
                exit()
            else:
                devmanufacturer = usb.util.get_string(dev, 1)
                devname = usb.util.get_string(dev, 2)
                print "device found: " + devmanufacturer + " " + devname
                interface = 0
                if dev.is_kernel_driver_active(interface) is True:
                    print "but we need to detach kernel driver"
                    dev.detach_kernel_driver(interface)
                    # use the first/default configuration
                    dev.set_configuration()
                    print "claiming device"
                    usb.util.claim_interface(dev, interface)
            while True:
                self.stable_weight = self.listen()
                if self.stable_weight != self.last_weight:
                    timestamp = datetime.now()
                    if not self.stable_weight == -3:
                        db.insert({"time": timestamp,"mass": self.stable_weight})
                        print "written"
                        self.last_weight = self.stable_weight


        except KeyboardInterrupt as e:
            print "\nquitting"
            exit();

    def grab(self):
        # first endpoint
        endpoint = dev[0][(0,0)][0]
        # read a data packet
        attempts = 10
        data = None
        while data is None and attempts > 0:
            try:
                data = dev.read(endpoint.bEndpointAddress,
                                endpoint.wMaxPacketSize)
            except usb.core.USBError as e:
                data = None
                if e.args == ('Operation timed out',):
                    attempts -= 1
                    print "timed out... trying again"
                    continue

        return data

    def listen(self):
        DATA_MODE_GRAMS = 2
        DATA_MODE_OUNCES = 11

        last_raw_weight = 0
        last_raw_weight_stable = 4

        print "listening for weight..."
        while True:
            time.sleep(.5)
            data = self.grab()
            if data != None:
                raw_weight = data[4] + data[5] * 256

            # +/- 2g
            if raw_weight > 0 and abs(raw_weight-last_raw_weight) > 0 and raw_weight != last_raw_weight:
                last_raw_weight_stable = 4
                last_raw_weight = raw_weight

            if raw_weight > 0 and last_raw_weight_stable >= 0:
                last_raw_weight_stable -= 1

            if raw_weight > 0 and last_raw_weight_stable == 0:
                if data[2] == DATA_MODE_OUNCES:
                    raw_weight = raw_weight * 2.83495
                elif data[2] == DATA_MODE_GRAMS:
                    raw_weight = raw_weight

                return raw_weight

#probe()
if __name__ == '__main__':
    scale = Scale()
    scale.main()

