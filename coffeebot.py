from flask import Flask
from flask import render_template
from coffee import coffee
import json

app = Flask(__name__)

app.debug = True


@app.route('/')
def hello_coffee():
    status = coffee.check_status()
    # print status, 'status'
    return render_template('index2.html', status=status)


@app.route('/day')
def day():
    json_result = json.dumps(coffee.history('day'))
    return json_result


@app.route('/week')
def week():
    json_result = json.dumps(coffee.history('week'))
    return json_result


@app.route('/month')
def month():
    json_result = json.dumps(coffee.history('year'))
    return json_result


@app.route('/year')
def year():
    json_result = json.dumps(coffee.history('month'))
    return json_result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
