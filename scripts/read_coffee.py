import pymongo
from datetime import datetime, timedelta
import numpy as np

current_status = -1
previous_status = -1

username = "rpi"
password = "passwordhere"

connection = pymongo.MongoClient("mongodb://{}:{}@candidate.19.mongolayer.com:10810/coffee".format(username,password))
db = connection.coffee.coffee_data

eod = datetime.now()
eod = eod.replace(hour=23,minute=0,second=0,microsecond=0)
sod = eod
sod = sod.replace(hour=5)

def history(timeframe):
	#Getting the datapoints for today
	if timeframe == "day":
		day_history = {}
		for item in db.find({"time": {"$lt": eod, "$gt":sod}}).sort("time"):
			day_history[item["time"]]=item["mass"]
		return day_history
	if timeframe == "local":
		day_history = []
		now = datetime.now()
		then = now - timedelta(minutes=5)
		for item in db.find({"time": {"$lt": now, "$gt":then}}).sort("time"):
			day_history.append([item["time"],item["mass"]])
		while len(day_history) == 0:
			#keep going back 5 minutes at a time till you find a value
			then = then - timedelta(minutes=5)
			for item in db.find({"time": {"$lt": now, "$gt":then}}).sort("time"):
				day_history.append([item["time"],item["mass"]])
		print day_history
		return day_history



def analyze(hist_list):
	hist_list_rev = hist_list 
	hist_list_rev.reverse()
	empty = [i for i in hist_list if i[1] <= 2200]
	low = [i for i in hist_list if i[1] <= 2600 and i[1] > 2200]
	full = [i for i in hist_list if i[1] >= 3500]
	if len(empty) > 0 and hist_list_rev[0][1] <= 2200:
		print "Probably Empty"
		return 0
	if len(low) > 0 and hist_list_rev[0][1] <= 2600 and hist_list_rev > 2200:
		print "We're Getting Low"
		return 1
	if len(full) > 0 and hist_list_rev[0][1] >= 3500:
		print "We're definiely full"
		return 2


#0 empty
#1 low
#2 full
#-1 init
#
def check_status():
	global current_status,previous_status
	#day_history = history("day")
	fivemin_history = history("local")
	current_status = analyze(fivemin_history)
	print current_status
	if current_status == -1:
		print "need more data, just booted"
		pass
	if current_status == 0:
		if previous_status == -1:
			## Just booted and have empty container"
			print "I'm alive! Lets make coffee!"
		if previous_status == 0:
			## Still Empty
			print "Still empty..."	
		if previous_status == 1:
			##Legitimate Empty - Notify
			print "yeah we're dry over here"
		if previous_status == 2:
			## From full to empty means something funky happened, probably
			print "We were just full - something might not be right"

	if current_status == 1:
		if previous_status == -1:
			## Just booted and have partially empty container"
			print "I'm alive! Lets make coffee! But rinse me out first"
		if previous_status == 0:
			## Partially filled - shouldn't be a real state...
			print "Somethings not right - I'm only half full!"
		if previous_status == 1:
			## Still just sitting here with coffee
			print "No one is drinking me..."
		if previous_status == 2:
			## From full to low is the expected transistion 
			print "We're getting low - keep an eye on me!"


	if current_status == 2:
		if previous_status == -1:
			## Just booted and have full container"
			print "I'm alive! I've got coffee! But it might be old..."
		if previous_status == 0:
			## refilled - this is correct state transistion
			print "What once was empty now is full - all is right in this world."
		if previous_status == 1:
			## Could have been old/stale - this is a potentially legit transistion
			print "I've got fresh coffee!"
		if previous_status == 2:
			## From full to full 
			print "All this fresh coffee and no one is drinking me!"
	previous_status = current_status

#db.find({"time": {"$lt": now, "$gt":recent}})
