FROM stellanetops/coffeebot:master
MAINTAINER Tyler Horvath
RUN sudo apt-get update && sudo apt-get install -y python-pip python-dev 
RUN mkdir -p /var/www/coffeebot
ADD . /var/www/coffeebot
RUN pip install -r /var/www/coffeebot/requirements.txt

CMD ["uwsgi", "--ini", "/var/www/coffeebot/coffeebot.ini"]

EXPOSE 5000
