var chartData = generatechartData(0);

function selectDataset(d) {
    var chartData = chartData[d];
}

function generatechartData(d) {

  $.ajax({
      type: "GET",
      dataType: 'json',
      url: "./day", // Passing a parameter to the API to specify number of days
    })
    .done(function( data ) {
      serial1 = []
      serial2 = []
      for(var i = 0; i < data.length; i++) {
            //console.log(data[i]["serial"]);
            if(data[i]["serial"] === "0081446059603") {
                serial1.push(data[i]);
            }
            else {
                serial2.push(data[i]);
            }
          
       }
      //console.log(data);
      //console.log(data.length);

      // When the response to the AJAX request comes back render the chart with new data
      var combined = [serial1,serial2];
      drawGraph(combined[d])
    })
    .fail(function() {
      // If there is no communication between the server, show an error
      alert( "error occured" );
    });
}

function drawGraph(chartData) {
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "light",
    "type": "serial",
    "marginRight": 80,
    "autoMarginOffset": 20,    
    "marginTop":20,
    "dataProvider": chartData,
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 2
    }],
    "graphs": [{
        "useNegativeColorIfDown": true,
        "balloonText": "[[category]]<br><b>value: [[value]]</b>",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletBorderColor": "#FFFFFF",
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "lineColor": "#fdd400",
        "negativeLineColor": "#67b7dc",
        "valueField": "value"
    }],
    "chartScrollbar": {
        "scrollbarHeight": 5,
        "backgroundAlpha": 0.1,
        "backgroundColor": "#868686",
        "selectedBackgroundColor": "#67b7dc",
        "selectedBackgroundAlpha": 1
    },
    "chartCursor": {
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true
    },
    "categoryField": "time",
    "categoryAxis": {
        "parseDates": true,
        "axisAlpha": 0,
        "minHorizontalGap": 60,
        "minPeriod":"mm"
    },
    "export": {
        "enabled": true
    }
});

}

